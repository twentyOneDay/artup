// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    '@nuxtjs/svg-sprite',
  ],
  css: ['~/assets/scss/main.scss']
})
